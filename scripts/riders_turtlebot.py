#!/usr/bin/env python

import rospy
import math
import sys

from std_msgs.msg import String
from geometry_msgs.msg import Twist
from gazebo_msgs.srv import GetModelState
from sensor_msgs.msg import Range

class Vector3:
    def __init__(self, x = 0, y = 0, z = 0):
        self.x = x
        self.y = y
        self.z = z

class Quaternion:
    def __init__(self, x = 0, y = 0, z = 0, w = 0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

class EulerAngles:

    def __init__(self, roll = 0, pitch = 0, yaw = 0):
        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw

    def quaternion_to_euler_angles(self, q):
        # https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        # roll (x-axis rotation)
        sinr_cosp = 2 * (q.w * q.x + q.y * q.z)
        cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y)
        self.roll = math.atan2(sinr_cosp, cosr_cosp)

        # pitch (y-axis rotation)
        sinp = 2 * (q.w * q.y - q.z * q.x)
        if abs(sinp) >= 1:
            self.pitch = math.copysign(math.pi/2, sinp)
        else:
            self.pitch = math.asin(sinp)

        # yaw (z-axis rotation)
        siny_cosp = 2 * (q.w * q.z + q.x * q.y)
        cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z)
        self.yaw = math.atan2(siny_cosp, cosy_cosp)


class RidersTurtlebot:
    def __init__(self, robot_name = "turtlebot"):        
    
        self.robot_name = robot_name

        self.__temp_angle = 0
        self.__i_angle = 0

        self.__angle_kp = 2
        self.__angle_ki = 0
        self.__angle_kd = 0

        self.msg = Twist()

        self.__middle_range = 0
        self.__front_left_range = 0
        self.__front_right_range = 0

        rospy.init_node("riders_turtlebot")
        self.__pub = rospy.Publisher("/cmd_vel/", Twist, queue_size=10)
        self.__pub.publish(self.msg)
        
        rospy.Subscriber("/turtlebot/front_left_sonar/front_left_range", Range, self.__callback)
        rospy.Subscriber("/turtlebot/front_right_sonar/front_right_range", Range, self.__callback)
        rospy.Subscriber("/turtlebot/middle_sonar/middle_range", Range, self.__callback)

        self.__flag = True
        self.__current_yaw = self.__get_current_yaw()
        self.__initial_yaw = self.__current_yaw

    def update(self):
        self.__pub.publish(self.msg)
        self.msg.angular.z = 0
        self.__flag = True

    def move_forward(self, velocity):
        self.__current_yaw = self.__get_current_yaw()
        angle = self.__map_angle(self.__initial_yaw) - self.__map_angle(self.__current_yaw)
        self.msg.angular.z = self.__get_pid_angle(angle)
        self.msg.linear.x = velocity

    def stop(self):
        null_msg = Twist()
        self.msg = null_msg

    def turn_left(self, rate):
        self.__current_yaw = self.__get_current_yaw()
        self.__initial_yaw = self.__map_angle(self.__current_yaw + rate)
        self.msg.angular.z = self.__map_angle(self.msg.angular.z + rate)

    def turn_right(self, rate):
        self.__current_yaw = self.__get_current_yaw()
        self.__initial_yaw = self.__map_angle(self.__current_yaw - rate)
        self.msg.angular.z = self.__map_angle(self.msg.angular.z - rate)

    def read_left_distance_sensor(self):
        return self.__front_left_range

    def read_right_distance_sensor(self):
        return self.__front_right_range

    def read_middle_distance_sensor(self):
        return self.__middle_range

    def __map_angle(self, angle):
        if angle >= math.pi:
            return -math.pi + (angle % math.pi)
        elif angle <= - math.pi:
            return math.pi - ((-angle) % math.pi)
        else:
            return angle

    def __callback(self, data):
        if data.header.frame_id == "middle_sonar_sensor_frame":
            self.__middle_range = data.range
        elif data.header.frame_id == "front_right_sonar_sensor_frame":
            self.__front_right_range = data.range
        elif data.header.frame_id == "front_left_sonar_sensor_frame":
            self.__front_left_range = data.range
        
    def __call_get_model_state(self, model_name):
        rospy.wait_for_service('/gazebo/get_model_state')
        try:
            get_model_state = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            response = get_model_state(model_name, None)
            return response
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
    
    def __get_current_yaw(self):
        if self.__flag:
            model_state = self.__call_get_model_state(self.robot_name)
            q = Quaternion(model_state.pose.orientation.x, model_state.pose.orientation.y, model_state.pose.orientation.z, model_state.pose.orientation.w)
            e = EulerAngles()
            e.quaternion_to_euler_angles(q)
            self.__flag = False
            return  e.yaw
        return self.__current_yaw

    def __get_pid_angle(self,angle):
        self.__i_angle = self.__map_angle(angle + self.__i_angle)
        result_angle = self.__angle_kp*angle + self.__angle_ki*self.__i_angle + self.__angle_kd*(angle-self.__temp_angle)
        self._temp_angle = angle
        return self.__map_angle(result_angle)