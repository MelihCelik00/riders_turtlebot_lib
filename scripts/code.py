#!/usr/bin/env python
import riders_turtlebot
import time

#setup
turtlebot = riders_turtlebot.RidersTurtlebot("robot")

#loop
while True:
    left_sensor = turtlebot.read_left_distance_sensor()
    right_sensor = turtlebot.read_right_distance_sensor()
    middle_sensor = turtlebot.read_middle_distance_sensor()
    error = left_sensor-right_sensor
    
    if ((middle_sensor or right_sensor or left_sensor) != (0.0 or 5.0)):
        if ((right_sensor == left_sensor) and (middle_sensor != (0.0 or 5.0))):
            turtlebot.move_forward(2)
            time.sleep(0.2)
        elif ((left_sensor and middle_sensor == 0.0) and right_sensor != (0.0 or 5.0)):
            while (middle_sensor == (0.0 or 5.0)):
                turtlebot.turn_right(2)
                turtlebot.move_forward(0.3)
                time.sleep(0.2)
            if (middle_sensor != (0.0 or 5.0)):
                turtlebot.move_forward(2)
                time.sleep(0.2)
        elif ((right_sensor and middle_sensor == 0.0) and left_sensor != (0.0 or 5.0)):
            while (middle_sensor == (0.0 or 5.0)):
                turtlebot.turn_left(2)
                turtlebot.move_forward(0.3)
                time.sleep(0.2)
            if (middle_sensor != (0.0 or 5.0)):
                turtlebot.move_forward(2)
                time.sleep(0.2)
                
    else:
        turtlebot.turn_right(2)
        turtlebot.move_forward(1)
        time.sleep(0.2)
    
    turtlebot.update()

    """
    if middle_sensor > 0.5:
        turtlebot.move_forward(1)
        if error < 0:
            turtlebot.turn_right(-error)
        else:
            turtlebot.turn_left(error)
    elif middle_sensor < 0.3:
        turtlebot.stop()
    else:
        turtlebot.move_forward(middle_sensor)
    print("middle: ",middle_sensor)
    print("left: ",left_sensor)
    print("right: ",right_sensor)
    time.sleep(0.5)
    turtlebot.update()
    """